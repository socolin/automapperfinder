﻿using System.Runtime.InteropServices;
using AutoMapper;

namespace ExampleProject
{
    public class ModelToContractProfile : Profile
    {
        public ModelToContractProfile()
        {
            CreateMap<Source, Destination>()
                .ForMember(x => x.NewId, opt => opt.MapFrom(s => s.Id));

            CreateMap<SourceGlobal, Source>()
                .ForMember(x => x.Id, opt => opt.MapFrom(s => s.Value));

            CreateMap<Destination, Source>()
                .ForMember(x => x.Id, opt => opt.MapFrom(s => s.NewId));

            CreateMap<string, Destination>()
                .ForMember(x => x.NewId, opt => opt.MapFrom(s => s.Length));
        }
    }
}