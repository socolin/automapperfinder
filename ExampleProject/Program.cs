﻿using System;
using System.Collections.Generic;
using AutoMapper;

namespace ExampleProject
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile(new ModelToContractProfile()));
            var mapper = config.CreateMapper();

            var profileMapping1 = mapper.Map<Destination>(new Source {Id = 42});
            var profileMapping2 = mapper.Map<Source>(new Destination {NewId = 42});
            var profileMapping3 = mapper.Map<Destination>("hello");
            var profileMappingList = mapper.Map<List<Destination>>(new List<Source> {new Source {Id = 42}});

            var sources = new[]
            {
                new Source {Id = 5},
                new Source {Id = 6},
                new Source {Id = 7}
            };

            var mappingIEnumerable = mapper.Map<IEnumerable<Destination>>(sources);
            var mappingEnumerable = mapper.Map<ICollection<Destination>>(sources);
            var mappingIList = mapper.Map<IList<Destination>>(sources);
            var mappingList = mapper.Map<List<Destination>>(sources);
            var mappingArray = mapper.Map<Destination[]>(sources);

            var globalSources = new[]
            {
                new SourceGlobal {Value = 5},
                new SourceGlobal {Value = 6},
                new SourceGlobal {Value = 7}
            };

            Mapper.Initialize(cfg => cfg.CreateMap<SourceGlobal, DestinationGlobal>());

            var globalMapping = Mapper.Map<SourceGlobal, DestinationGlobal>(new SourceGlobal {Value = 42});
            var globalMappingIEnumerable = Mapper.Map<SourceGlobal[], IEnumerable<DestinationGlobal>>(globalSources);
            var globalMappingEnumerable = Mapper.Map<SourceGlobal[], ICollection<DestinationGlobal>>(globalSources);
            var globalMappingIList = Mapper.Map<SourceGlobal[], IList<DestinationGlobal>>(globalSources);
            var globalMappingList = Mapper.Map<SourceGlobal[], List<DestinationGlobal>>(globalSources);
            var globalMappingArray = Mapper.Map<SourceGlobal[], DestinationGlobal[]>(globalSources);

            Mapper.Initialize(cfg => cfg.CreateMap<SourceGlobal, Source>());
            var multipleDef = Mapper.Map<SourceGlobal, Source>(new SourceGlobal {Value = 42});

        }
    }
}