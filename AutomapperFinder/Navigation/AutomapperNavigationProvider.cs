using System;
using System.Collections.Generic;
using System.Linq;
using AutomapperFinder.Caching;
using JetBrains.Application.DataContext;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Feature.Services.Navigation.ContextNavigation;
using JetBrains.ReSharper.Feature.Services.Occurrences;
using JetBrains.ReSharper.Features.Navigation.Features.GoToDeclaredElement;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Files;
using JetBrains.ReSharper.Psi.Tree;
using JetBrains.Util;

namespace AutomapperFinder.Navigation
{
    [ContextNavigationProvider]
    public class AutomapperNavigationProvider : INavigateFromHereProvider
    {
        public IEnumerable<ContextNavigation> CreateWorkflow(IDataContext dataContext)
        {
            var invocationExpression = GetParentInvocationExpression(dataContext.GetSelectedTreeNode<ITreeNode>());

            if (!IsInvocationToMap(invocationExpression, out var sourceType, out var destType))
                yield break;

            var solution = invocationExpression.GetSolution();
            var psiServices = invocationExpression.GetPsiServices();
            var mapDefinitionCache = solution.GetComponent<AutomapperMapDefinitionsCache>();

            if (mapDefinitionCache.GetMap(sourceType, destType).Count == 0)
                yield break;

            yield return new ContextNavigation(
                "&Automapper mapping definition",
                "AutomapperGoToMapDefinition",
                NavigationActionGroup.Blessed,
                () => {
                    var elements = mapDefinitionCache.GetMap(sourceType, destType);

                    if (elements.Count == 0)
                    {
                        MessageBox.ShowInfo($"Mapping {sourceType} -> {destType} not found.");
                        return;
                    }

                    var occurrencePopupMenu = solution.GetComponent<OccurrencePopupMenu>();
                    var occurrences = new List<IOccurrence>();

                    foreach (var element in elements)
                    {
                        var node = FindDefinition(psiServices, element, destType, sourceType);
                        if (node != null)
                            occurrences.Add(new ReferenceOccurrence(node, OccurrenceType.Occurrence));
                    }

                    if (elements.Count == 0)
                    {
                        MessageBox.ShowInfo($"Mapping {sourceType} -> {destType} not found, but present in cache.");
                        return;
                    }

                    occurrencePopupMenu.ShowMenuFromTextControl(
                        dataContext,
                        occurrences,
                        () => new GotoDeclaredElementsBrowserDescriptor(solution, null, string.Empty, occurrences, () => occurrences),
                        OccurrencePresentationOptions.DefaultOptions,
                        true,
                        "Goto mapping definition"
                    );
                },
                icon: UnnamedThemedIcons.AutomapperIcon.Id
            );
        }


        private static IInvocationExpression FindDefinition(IPsiServices psiServices, Tuple<IPsiSourceFile, string> def, string destType, string sourceType)
        {
            var types = psiServices.Symbols.GetTypesAndNamespacesInFile(def.Item1);
            foreach (var decElement in types)
            {
                if (!(decElement is IClass cl))
                    continue;
                if (cl.GetClrName().FullName != def.Item2)
                    continue;

                var cSharpFile = def.Item1.GetPsiFiles<CSharpLanguage>().OfType<ICSharpFile>().FirstOrDefault();

                var node = SearchAutomapperReference(cSharpFile?.Root(), destType, sourceType);
                if (node?.Reference != null)
                {
                    return node;
                }
            }

            return null;
        }

        private IInvocationExpression GetParentInvocationExpression(ITreeNode node)
        {
            while (node != null)
            {
                if (node is IInvocationExpression invocationExpression)
                    return invocationExpression;
                node = node.Parent;
            }

            return null;
        }

        private bool IsInvocationToMap(IInvocationExpression invocationExpression, out string sourceType, out string destType)
        {
            sourceType = null;
            destType = null;

            if (invocationExpression == null)
                return false;

            if (!(invocationExpression.InvocationExpressionReference.Resolve().DeclaredElement is IClrDeclaredElement declaredElement))
                return false;

            if (declaredElement.Module.Name != "AutoMapper")
                return false;

            if (declaredElement.ShortName != "Map")
                return false;

            var objectClassName = declaredElement.GetContainingType()?.ShortName;
            if (objectClassName != "IMapper" && objectClassName != "Mapper")
                return false;

            switch (invocationExpression.TypeArguments.Count)
            {
                case 1:
                    sourceType = invocationExpression.ArgumentsEnumerable.First().GetExpressionType().ToIType()?.GetLongPresentableName(CSharpLanguage.Instance);
                    destType = invocationExpression.TypeArguments.First().GetLongPresentableName(CSharpLanguage.Instance);
                    break;
                case 2:
                    sourceType = invocationExpression.TypeArguments[0].GetLongPresentableName(CSharpLanguage.Instance);
                    destType = invocationExpression.TypeArguments[1].GetLongPresentableName(CSharpLanguage.Instance);
                    break;
                default:
                    return false;
            }

            destType = GetCollectionInnerType(destType);
            sourceType = GetCollectionInnerType(sourceType);

            return true;
        }

        private string GetCollectionInnerType(string type)
        {
            if (type.EndsWith("[]"))
            {
                return type.Substring(0, type.Length - 2);
            }

            var genericTypesNames = new[]
            {
                "System.Collections.Generic.ICollection",
                "System.Collections.Generic.IEnumerable",
                "System.Collections.Generic.IList",
                "System.Collections.Generic.List"
            };

            foreach (var typeName in genericTypesNames)
            {
                if (type.StartsWith(typeName + "<") && type.EndsWith(">"))
                {
                    return type.Substring(typeName.Length + 1, type.Length - 2 - typeName.Length);
                }
            }

            return type;
        }

        private static IInvocationExpression SearchAutomapperReference(ITreeNode root, string destType,
            string sourceType)
        {
            var node = root.FirstChild;
            while (node != null)
            {
                if (node is IInvocationExpression invocationExpression)
                {
                    if ((invocationExpression.InvocationExpressionReference.Resolve().DeclaredElement is
                        IClrDeclaredElement declaredElement))
                    {
                        if (declaredElement.ShortName == "CreateMap")
                        {
                            if (invocationExpression.TypeArguments.Count == 2)
                            {
                                var firstType = invocationExpression.TypeArguments[0].GetLongPresentableName(CSharpLanguage.Instance);
                                var secondType = invocationExpression.TypeArguments[1].GetLongPresentableName(CSharpLanguage.Instance);

                                if (firstType.Equals(sourceType, StringComparison.Ordinal) && secondType.Equals(destType, StringComparison.Ordinal))
                                {
                                    return invocationExpression;
                                }
                            }
                        }
                    }
                }

                var result = SearchAutomapperReference(node, destType, sourceType);
                if (result != null)
                {
                    return result;
                }

                node = node.NextSibling;
            }

            return null;
        }
    }
}