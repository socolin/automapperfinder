using JetBrains.Application.UI.ActionSystem.ActionsRevised.Menu;
using JetBrains.ReSharper.Feature.Services.Navigation.ContextNavigation;

namespace AutomapperFinder.Navigation
{
    [ActionHandler(typeof(AutomapperNavigation))]
    public class AutomapperNavigation : ContextNavigationActionBase<AutomapperNavigationProvider>
    {
    }
}