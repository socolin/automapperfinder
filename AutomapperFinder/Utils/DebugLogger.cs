using System.Net;
using System.Net.Sockets;
using System.Text;
using JetBrains.ReSharper.Psi.Tree;

namespace AutomapperFinder.Utils
{
    public static class DebugLogger
    {
        public static bool Debug { get; set; } = true;
        private const string IndentString = "  ";

        public static void Log(string message)
        {
            if (!Debug)
                return;

            using (var sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
            {
                var serverAddr = IPAddress.Parse("127.0.0.1");
                var endPoint = new IPEndPoint(serverAddr, 42421);
                var buffer = Encoding.UTF8.GetBytes(message + "\n");
                sock.SendTo(buffer, endPoint);
            }
        }

        public static void PrintTree(ITreeNode root, string indent = "")
        {
            if (root == null)
            {
                Log(indent + "null");
                return;
            }

            Log(indent + root.NodeType + " - " + root.GetText());
            var node = root.FirstChild;
            while (node != null)
            {
                PrintTree(node, indent + IndentString);
                node = node.NextSibling;
            }
        }
    }
}