using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using JetBrains.Application.Progress;
using JetBrains.DataFlow;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.Caches;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Files;
using JetBrains.ReSharper.Psi.Tree;
using JetBrains.Util;

namespace AutomapperFinder.Caching
{
    [PsiComponent]
    public class AutomapperMapDefinitionsCache : SimpleICache<AutomapperMapDefinitions>
    {
        private AutomapperMapDefinitionsMergeData _mergeData;

        public override string Version => "5";

        public OneToSetMap<Tuple<string, string>, Tuple<IPsiSourceFile, string>> AllDefinition => _mergeData.MapDefinitions;

        public AutomapperMapDefinitionsCache(Lifetime lifetime, IPersistentIndexManager persistentIndexManager)
            : base(lifetime, persistentIndexManager, new AutomapperMapDefinitionMarshaller())
        {
        }

        public ISet<Tuple<IPsiSourceFile, string>> GetMap(string sourceType, string destType)
        {
            var definitionType = Tuple.Create(sourceType, destType);
            return _mergeData.MapDefinitions[definitionType];
        }

        public override object Load(IProgressIndicator progress, bool enablePersistence)
        {
            var data = new AutomapperMapDefinitionsMergeData();

            foreach (var x in Map.Where(x => x.Key != null))
                LoadFile(data, x.Key, x.Value);

            return data;
        }

        public override void MergeLoaded(object data)
        {
            _mergeData = (AutomapperMapDefinitionsMergeData) data;

            base.MergeLoaded(data);
        }

        [NotNull]
        public override object Build(IPsiSourceFile sourceFile, bool isStartup)
        {
            var file = sourceFile.GetPrimaryPsiFile().NotNull();
            if (!UseAutomapper(file))
            {
                return new AutomapperMapDefinitions();
            }

            var mapDefinitions = new AutomapperMapDefinitions();
            foreach (var type in GetTypeDeclarations(file))
            {
                if (!(type.DeclaredElement is IClass cl))
                    continue;

                foreach (var mapDefinition in ListMapDefinitions(type))
                    mapDefinitions.Add(mapDefinition, cl.GetClrName().FullName);
            }

            return mapDefinitions;
        }

        private static bool UseAutomapper(IFile node)
        {
            if (!(node is ICSharpFile cSharpFile))
                return false;

            foreach (var usingDirective in cSharpFile.Imports)
                if (usingDirective.ImportedSymbolName.QualifiedName == "AutoMapper")
                    return true;

            return false;
        }

        public override void Merge(IPsiSourceFile sourceFile, object builtPart)
        {
            if (builtPart == null)
                return;

            var mapDefinitions = (AutomapperMapDefinitions) builtPart;

            base.Merge(sourceFile, builtPart);

            RemoveDataOfFile(sourceFile);
            LoadFile(_mergeData, sourceFile, mapDefinitions);
        }

        public override void Drop(IPsiSourceFile sourceFile)
        {
            RemoveDataOfFile(sourceFile);
            base.Drop(sourceFile);
        }

        protected override bool IsApplicable([NotNull] IPsiSourceFile sourceFile)
        {
            return sourceFile.GetLanguages().Any(l => l.IsLanguage(CSharpLanguage.Instance));
        }

        private void RemoveDataOfFile(IPsiSourceFile sourceFile)
        {
            if (!_mergeData.MapDefinitionsPerFiles.ContainsKey(sourceFile))
                return;
            foreach (var definition in _mergeData.MapDefinitionsPerFiles[sourceFile])
            {
                var def = _mergeData.MapDefinitions[definition].FirstOrDefault(d => d.Item1 == sourceFile);
                if (def != null)
                    _mergeData.MapDefinitions.Remove(definition, def);
            }

            _mergeData.MapDefinitionsPerFiles.RemoveKey(sourceFile);
        }

        private IEnumerable<ITypeDeclaration> GetTypeDeclarations(ITreeNode node)
        {
            if (node is INamespaceDeclarationHolder namespaceDeclarationHolder)
                foreach (var typeDeclaration in namespaceDeclarationHolder.NamespaceDeclarations.SelectMany(GetTypeDeclarations))
                    yield return typeDeclaration;

            if (node is ITypeDeclarationHolder typeDeclarationHolder)
                foreach (var typeDeclaration in typeDeclarationHolder.TypeDeclarations)
                    yield return typeDeclaration;
        }


        private static void LoadFile(AutomapperMapDefinitionsMergeData data, IPsiSourceFile sourceFile, AutomapperMapDefinitions mapDefinitions)
        {
            var mapDefinitionTypes = mapDefinitions.Keys;
            data.MapDefinitionsPerFiles.AddRange(sourceFile, mapDefinitionTypes);

            foreach (var definitionType in mapDefinitionTypes)
            {
                foreach (var mapDefinition in mapDefinitions[definitionType])
                {
                    data.MapDefinitions.Add(definitionType, Tuple.Create(sourceFile, mapDefinition));
                }
            }
        }

        private static bool IsMapDefinition(IInvocationExpression invocationExpression)
        {
            if (!(invocationExpression.InvocationExpressionReference.Resolve().DeclaredElement is IClrDeclaredElement declaredElement))
                return false;

            if (declaredElement.ShortName != "CreateMap")
                return false;

            if (invocationExpression.TypeArguments.Count != 2)
                return false;

            return true;
        }

        private static IEnumerable<Tuple<string, string>> ListMapDefinitions(ITreeNode root)
        {
            var node = root.FirstChild;

            while (node != null)
            {
                if (node is IInvocationExpression invocationExpression)
                {
                    if (IsMapDefinition(invocationExpression))
                    {
                        var srcType = invocationExpression.TypeArguments[0].GetLongPresentableName(CSharpLanguage.Instance);
                        var dstType = invocationExpression.TypeArguments[1].GetLongPresentableName(CSharpLanguage.Instance);

                        yield return Tuple.Create(srcType, dstType);
                    }
                }

                foreach (var mapDefinition in ListMapDefinitions(node))
                {
                    yield return mapDefinition;
                }

                node = node.NextSibling;
            }
        }
    }
}