using System;
using System.Linq;
using JetBrains.Annotations;
using JetBrains.Util.PersistentMap;

namespace AutomapperFinder.Caching
{
    public class AutomapperMapDefinitionMarshaller : IUnsafeMarshaller<AutomapperMapDefinitions>
    {
        public void Marshal([NotNull] UnsafeWriter writer, [NotNull] AutomapperMapDefinitions value)
        {
            writer.Write(value.Count);
            foreach (var map in value)
            {
                writer.Write(map.Key.Item1);
                writer.Write(map.Key.Item2);
                writer.Write(map.Value.Count);
                foreach (var name in map.Value)
                    writer.Write(name);
            }
        }

        public AutomapperMapDefinitions Unmarshal([NotNull] UnsafeReader reader)
        {
            var linkData = new AutomapperMapDefinitions();
            var count = reader.ReadInt();
            for (var i = 0; i < count; i++)
            {
                var sourceType = reader.ReadString();
                var destType = reader.ReadString();
                var listCount = reader.ReadInt();
                var linkedTypes = Enumerable.Range(start: 0, count: listCount).Select(x => reader.ReadString());
                linkData.AddValueRange(Tuple.Create(sourceType, destType), linkedTypes);
            }

            return linkData;
        }
    }
}