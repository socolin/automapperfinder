using System;
using JetBrains.ReSharper.Psi;
using JetBrains.Util;

namespace AutomapperFinder.Caching
{
    public class AutomapperMapDefinitionsMergeData
    {
        public readonly OneToSetMap<Tuple<string, string>, Tuple<IPsiSourceFile, string>> MapDefinitions = new OneToSetMap<Tuple<string, string>, Tuple<IPsiSourceFile, string>>();
        public readonly OneToSetMap<IPsiSourceFile, Tuple<string, string>> MapDefinitionsPerFiles = new OneToSetMap<IPsiSourceFile, Tuple<string, string>>();
    }
}